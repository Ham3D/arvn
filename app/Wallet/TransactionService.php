<?php

namespace App\Wallet;

use App\Enums\TransactionTypesEnum;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Voucher;
use App\Models\Wallet;
use DB;
use Illuminate\Support\Collection;

class TransactionService
{
    public static function getWalletTransactions(Wallet $wallet): Collection
    {
        return Transaction::whereWalletId($wallet->id)->get();
    }

    public static function addClaimedVoucherTransactions(Voucher $voucher, User $user): void
    {
        DB::transaction(function () use ($voucher, $user) {
            $userWallet = WalletService::getUserWallet(user: $user);
            $systemWallet = WalletService::getSystemWallet();

            Transaction::create([
                'amount' => $voucher->value,
                'type' => TransactionTypesEnum::Withdraw->name,
                'wallet_id' => $systemWallet->id,
            ]);

            Transaction::create([
                'amount' => $voucher->value,
                'type' => TransactionTypesEnum::Deposit->name,
                'wallet_id' => $userWallet->id,
            ]);
        });
    }

    public static function updateWallet(Transaction $transaction): void
    {
        switch ($transaction->type) {
            case TransactionTypesEnum::Deposit->name:
                $transaction->wallet()->increment('balance', $transaction->amount);
                break;
            case TransactionTypesEnum::Withdraw->name:
                $transaction->wallet()->decrement('balance', $transaction->amount);
                break;
        }
    }
}
