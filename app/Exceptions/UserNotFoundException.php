<?php

namespace App\Exceptions;

use Illuminate\Http\JsonResponse;

class UserNotFoundException extends Exception
{
    public function render(): JsonResponse
    {
        return $this->error(404, 'UserNotFound');
    }
}
