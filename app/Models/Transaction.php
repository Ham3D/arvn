<?php

namespace App\Models;

use App\Wallet\TransactionService;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends BaseModel
{
    public static function boot(): void
    {
        parent::boot();

        self::created(function (Transaction $transaction) {
            TransactionService::updateWallet(transaction: $transaction);
        });
    }

    public function wallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class);
    }
}
