<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;

class Wallet extends BaseModel
{
    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }
}
