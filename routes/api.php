<?php

use App\Http\Controllers\ReportController;
use App\Http\Controllers\VoucherController;
use App\Http\Controllers\WalletController;
use Illuminate\Support\Facades\Route;

Route::get('voucher', [VoucherController::class, 'index'])->name('voucher.index');
Route::post('voucher', [VoucherController::class, 'store'])->name('voucher.store');
Route::put('voucher/{id}', [VoucherController::class, 'update'])->name('voucher.update');
Route::delete('voucher/{id}', [VoucherController::class, 'destroy'])->name('voucher.destroy');
Route::get('voucher/claim', [VoucherController::class, 'claim'])->name('voucher.claim');
Route::get('voucher/{id}', [VoucherController::class, 'show'])->name('voucher.show');

Route::get('wallet', [WalletController::class, 'balance'])->name('wallet.credit');
Route::get('wallet/transactions', [WalletController::class, 'transactions'])->name('wallet.transactions');

Route::get('report', ReportController::class)->name('report');
