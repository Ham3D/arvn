<?php

use App\Models\User;
use App\Models\Voucher;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('user_voucher', function (Blueprint $table) {
            $table->foreignIdFor(User::class)->constrained();
            $table->foreignIdFor(Voucher::class)->constrained();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::table('user_voucher', function (Blueprint $table) {
            Schema::dropIfExists('voucher_user');
        });
    }
};
