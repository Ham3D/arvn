<?php

namespace App\Http\Controllers;

use App\Enums\VoucherTypesEnum;
use App\Exceptions\ExpirationDateIsAlreadyPassedException;
use App\Exceptions\VoucherAlreadyUsedException;
use App\Exceptions\VoucherCodeExistException;
use App\Exceptions\VoucherIsNotAvailableException;
use App\Http\Requests\ClaimVoucherRequest;
use App\Http\Requests\VoucherIndexRequest;
use App\Http\Requests\VoucherStoreRequest;
use App\Http\Requests\VoucherUpdateRequest;
use App\Http\Resources\VoucherResource;
use App\Voucher\VoucherService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class VoucherController extends Controller
{
    /**
     * @param  VoucherIndexRequest  $request
     * @param  VoucherService  $voucherService
     * @return AnonymousResourceCollection
     */
    public function index(VoucherIndexRequest $request, VoucherService $voucherService): AnonymousResourceCollection
    {
        return $voucherService->getVouchers(request: $request);
    }

    /**
     * @param  VoucherStoreRequest  $request
     * @param  VoucherService  $voucherService
     * @return bool
     *
     * @throws ExpirationDateIsAlreadyPassedException
     * @throws VoucherCodeExistException
     */
    public function store(VoucherStoreRequest $request, VoucherService $voucherService): bool
    {
        return $voucherService->generate(
            type: VoucherTypesEnum::from(value: $request->get('type')),
            value: $request->get('value'),
            quantity: $request->get('quantity') ?? 1,
            expireAt: $request->get('expire_at'),
            code: $request->get('code')
        );
    }

    /**
     * @param    $id
     * @param  VoucherService  $voucherService
     * @return VoucherResource
     */
    public function show($id, VoucherService $voucherService): VoucherResource
    {
        return $voucherService->getVoucher(id: $id);
    }

    /**
     * @param  VoucherUpdateRequest  $request
     * @param    $id
     * @param  VoucherService  $voucherService
     * @return bool
     */
    public function update(VoucherUpdateRequest $request, $id, VoucherService $voucherService): bool
    {
        return $voucherService->update(request: $request, id: $id);
    }

    /**
     * vouchers are not destroyable
     * we can disable them
     *
     * @param    $id
     * @param  VoucherService  $voucherService
     * @return bool
     */
    public function destroy($id, VoucherService $voucherService): bool
    {
        return $voucherService->destroy(id: $id);
    }

    /**
     * @throws VoucherIsNotAvailableException
     * @throws VoucherAlreadyUsedException
     */
    public function claim(ClaimVoucherRequest $request, VoucherService $voucherService): bool
    {
        return $voucherService->claimVoucher(request: $request);
    }
}
