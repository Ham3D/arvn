<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserVoucherPivot extends Pivot
{
    protected $table = 'user_voucher';
}
