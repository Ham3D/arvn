<?php

namespace Tests\Feature;

use App\Enums\TransactionTypesEnum;
use App\Enums\VoucherTypesEnum;
use App\Exceptions\ExpirationDateIsAlreadyPassedException;
use App\Exceptions\SystemIntegrityException;
use App\Exceptions\VoucherCodeExistException;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Voucher;
use App\Voucher\VoucherService;
use App\Wallet\WalletService;
use Tests\TestCase;

class WalletTest extends TestCase
{
    /**
     * @test
     */
    public function itGetUserBalance(): void
    {
        // Arrange
        $user = User::factory()->create();

        // Act
        $request = $this->get(route('wallet.credit', ['phone' => $user->phone]));

        // Assert
        $request->assertJsonFragment(['user_id' => $user->id]);
        $request->assertJsonFragment(['balance' => 0]);
    }

    /**
     * @test
     *
     * @throws VoucherCodeExistException
     * @throws ExpirationDateIsAlreadyPassedException
     */
    public function itGetWalletTransactions(): void
    {
        // Arrange
        $user = User::factory()->create();
        $voucherService = new VoucherService();
        $voucherService->generate(type: VoucherTypesEnum::FixedAmount, value: 1000);
        $voucher = Voucher::first();
        $userWalletId = WalletService::getUserWallet(user: $user);

        // Act
        $this->get(route('voucher.claim', [
            'id' => $voucher->id,
            'phone' => $user->phone,
            'voucher' => $voucher->code,
        ]));

        $response = $this->get(route('wallet.transactions', [
            'phone' => $user->phone,
        ]));

        // Assert
        $response->assertJsonFragment(['type' => TransactionTypesEnum::Deposit->name]);
        $response->assertJsonFragment(['amount' => 1000]);
        $response->assertJsonFragment(['wallet_id' => $userWalletId->id]);
    }

    /**
     * @test
     *
     * @throws VoucherCodeExistException
     * @throws ExpirationDateIsAlreadyPassedException
     */
    public function itCanCheckForSystemIntegrity(): void
    {
        // Arrange
        $user = User::factory()->create();
        $voucherService = new VoucherService();
        $voucherService->generate(type: VoucherTypesEnum::FixedAmount, value: 1000);

        $voucher = Voucher::first();

        // Act
        $this->get(route('voucher.claim', [
            'id' => $voucher->id,
            'phone' => $user->phone,
            'voucher' => $voucher->code,
        ]));

        // Assert
        $this->assertTrue(WalletService::checkTotalSystemIntegrity());
    }

    /**
     * @test
     *
     * @throws VoucherCodeExistException
     * @throws ExpirationDateIsAlreadyPassedException
     */
    public function itDetectFraudWhenWeChangeTransactions(): void
    {
        // Arrange
        $user = User::factory()->create();
        $voucherService = new VoucherService();
        $voucherService->generate(type: VoucherTypesEnum::FixedAmount, value: 1000);
        $voucher = Voucher::first();

        // Act
        $this->get(route('voucher.claim', [
            'id' => $voucher->id,
            'phone' => $user->phone,
            'voucher' => $voucher->code,
        ]));

        $firstTransaction = Transaction::first();
        $firstTransaction->increment('amount', 100);

        // Assert
        $this->assertFalse(WalletService::checkTotalSystemIntegrity());
    }

    /**
     * @test
     *
     * @throws VoucherCodeExistException
     * @throws ExpirationDateIsAlreadyPassedException
     */
    public function itDetectFraudOnChangingWallets(): void
    {
        // Assert
        $this->expectException(SystemIntegrityException::class);

        // Arrange
        $user = User::factory()->create();
        $voucherService = new VoucherService();
        $voucherService->generate(type: VoucherTypesEnum::FixedAmount, value: 1000);

        $voucher = Voucher::first();

        // Act
        $this->get(route('voucher.claim', [
            'id' => $voucher->id,
            'phone' => $user->phone,
            'voucher' => $voucher->code,
        ]));
        $userWallet = WalletService::getUserWallet(user: $user);
        $userWallet->increment('balance', 10000);

        WalletService::checkWalletsIntegrity();
    }
}
