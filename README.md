## چالش طراحی سیستم
چالش اصلی در این سیستم (و به نظر من نقطه اصلی مورد نظر) بخش مدیریت درخواست‌ها برای فعال‌کردن کد تخفیف در لود بالاست.
در زیر بعضی از روش‌های ممکن برای حل این مساله و چالش‌های هرکدام رو به اختصار بیان می‌کنم
- استفاده از یک رکورد برای ذخیره تعداد مجاز کد تخفیف(یا شارژ) و کم کردن (آپدیت کردن) این رکورد بعد از هر درخواست کاربر
	- در صورتی که سیستم لود پایینی داشته باشد این روش قابل انجام است
	-در لودهای بالاتر مشکل زیر به وجود می‌آید
	- race condition
	- به این معنی که اگر تعداد اتصالات به دیتابیس بیشتر و بیشتر شود، دیتابیس ردیف مورد نظر (یا حتی جدول مورد نظر در بعضی از انواع جدول‌ها) را برای آپدیت قفل کرده و اجازه دسترسی به کاربران جدید نمیدهد
	- به این دلیل کاربران تایم اوت میخورن در صورتی زمان بگذره و پرفورمنس سیستم پایین است.
- روش بعدی اینکه در هر ردیف جدول ذخیره کدهای شارژ تعداد نداشته باشیم و به ازای هر تعداد از آن کد تخفیف یک رکورد جدید ایجاد کنیم
	- به نظر من بهترین روش این است.
	- با وجودی که داده های بیشتری تولید میشود، اما در این روش این تضمین وجود دارد که دقیقا به اندازه تعیین شده کاربران اجازه تایید کد تخفیف یا شارژ را دارند
	- سیستم پرفرومنس مناسب و اسکیل پذیری بسیار بالایی دارد
	- به نظر من در دنیای سیستم‌های اینترپرایز تنها این روش مناسب و جوابگو است
	- من از این روش استفاده نکردم و روش زیر رو توسعه دادم، اما خوب در جلسه حضوری در این مورد هم صحبت خواهیم کرد
- روشی که من از اون استفاده کردم، روی هر رکورد از کد تخفیف یا شارژ تعداد آن هم آمده است، اما این مطلب که آیا کاربر آن را استفاده کرده یا نه به این جدول  ارتباطی ندارد
	- استفاده کاربر از کد تخفیف در جدول دیگری ذخیره میشود
	- به دلیل آپدیت نکردن رکورد اصلی کد تخفیف، در این روش مشکل زیر را نداریم
	- race condition
	- در این روش امکان دارد به اندازه تعداد ترد هایی از دیتابیس که فعال هستند کد تخفیف بیشتر استفاده شود
	- این روش من حیث المجموع روش مناسبی است و در کنار کارایی مناسب پرفورمنس بالایی هم دارد
	- البته مشکلات خود را هم دارد که در بالا به آن اشاره شد
	- در نهایت به نظر نمی‌رسد چند استفاده بیشتر از کد تخفیف در بیزنس مشکلی ایجاد نماید

## استفاده از سیستم
- برای تمام سرویس‌ها و متودهای مورد نیاز تست نوشته شده است.
- فایل روت جدا سازی نشده و همه روت ها در یک فایل آمده (برای سادگی کار)
- برای تمام خطاها اکسپشن ساخته شده
- در سیستم از موارد زیر استفاده شده
	- Enums
	- php 8.1
- استفاده از  ای نام ها مزایا و معایبی دارد که قابل بحث است
- تنها نوع قابل تعریف کد تخفیف یا شارژ - مقدار ثابت است (که مقدار تعریف شده را به کیف پول کاربر اضافه می‌کند)
- برای سیستم دو متد شناسایی خطا هم اضافه شده که در صورتی که مقدار کیف پول کاربران یا تراکنش‌ها به صورت مخرب تغییر داده شود مشکل را شناسایی نمایند. (تشخیص هک یا خرابکاری)
- با کمک دستورهای زیر میتوان سیستم را تست نمود
  - php artisan test
  - phpunit
- برای سیستم سیدر هم نوشته شده که با دستور زیر هم دیتابیس ساخته شده و هم کاربران سید میشوند
  - php artisan migrate:fresh --seed

---
در صورتی که مطلبی باقی مانده لطفا در جلسه مطرح شود.

این روزها به شدت زمان شلوغی دارم و این فایل نهایی رو ساعت 3:50 بامداد روز دوشنبه مینویسم.

با تشکر

Ham3D