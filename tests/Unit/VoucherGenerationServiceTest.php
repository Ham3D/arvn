<?php

namespace Tests\Unit;

use App\Enums\VoucherTypesEnum;
use App\Exceptions\ExpirationDateIsAlreadyPassedException;
use App\Exceptions\VoucherCodeExistException;
use App\Models\Voucher;
use App\Voucher\VoucherService;
use Tests\TestCase;

class VoucherGenerationServiceTest extends TestCase
{
    /**
     * @throws ExpirationDateIsAlreadyPassedException
     * @throws VoucherCodeExistException
     * @test
     */
    public function itGenerateNewVoucher(): void
    {
        // Arrange
        $voucherGenerator = new VoucherService();

        // Act
        $voucherGenerator->generate(type: VoucherTypesEnum::FixedAmount, value: 1000);
        $voucher = Voucher::first();

        // Assert
        $this->assertEquals(1000, $voucher->value);
        $this->assertEquals(VoucherTypesEnum::FixedAmount->name, $voucher->type);
        $this->assertDatabaseHas('vouchers', [
            'value' => 1000,
        ]);
    }

    /**
     * @test
     */
    public function itGenerateUniqueCodes(): void
    {
        // Arrange
        // Act
        $code = Voucher::generateCode();

        // Assert
        $this->assertDatabaseMissing('vouchers', [
            'code' => $code,
        ]);
    }

    /**
     * @throws ExpirationDateIsAlreadyPassedException
     * @throws VoucherCodeExistException
     * @test
     */
    public function itGenerateVoucherWithExpirationDate(): void
    {
        // Arrange
        $voucherGenerator = new VoucherService();

        // Act
        $voucherGenerator->generate(type: VoucherTypesEnum::FixedAmount, value: 1000, expireAt: now()->addDays(10));

        // Assert
        $voucher = Voucher::first();
        $this->assertEquals(now()->addDays(10), $voucher->expires_at);
    }

    /**
     * @throws ExpirationDateIsAlreadyPassedException
     * @throws VoucherCodeExistException
     * @test
     */
    public function itGenerateVouchersWithSpecifiedQuantity(): void
    {
        // Arrange
        $voucherGenerator = new VoucherService();

        // Act
        $voucherGenerator->generate(type: VoucherTypesEnum::FixedAmount, value: 1000, quantity: 101);

        // Assert
        $voucher = Voucher::first();
        $this->assertEquals(101, $voucher->quantity);
    }

    /**
     * @throws ExpirationDateIsAlreadyPassedException
     * @throws VoucherCodeExistException
     * @test
     */
    public function itGenerateVoucherWithCustomCode(): void
    {
        // Arrange
        $voucherGenerator = new VoucherService();
        $customCode = 'custom-code';

        // Act
        $voucherGenerator->generate(type: VoucherTypesEnum::FixedAmount, value: 1000, code: $customCode);

        // Assert
        $voucher = Voucher::first();
        $this->assertEquals($customCode, $voucher->code);
    }

    /**
     * @test
     *
     * @throws ExpirationDateIsAlreadyPassedException
     */
    public function itCantGenerateVoucherWithDuplicateCodes(): void
    {
        // Assert
        $this->expectException(VoucherCodeExistException::class);

        // Arrange
        $voucherGenerator = new VoucherService();
        $customCode = 'custom-code';

        // Act
        $voucherGenerator->generate(type: VoucherTypesEnum::FixedAmount, value: 1000, code: $customCode);

        // generate second voucher with same code
        $voucherGenerator->generate(type: VoucherTypesEnum::FixedAmount, value: 200, code: $customCode);
    }
}
