<?php

namespace Tests\Unit;

use App\Models\User;
use App\Wallet\WalletService;
use Tests\TestCase;

class WalletTest extends TestCase
{
    /**
     * @test
     */
    public function itGetUserWallet(): void
    {
        // Arrange
        $user = User::factory(['phone' => 123])->create();

        // Act
        $userWallet = (new WalletService())->getUserWallet(user: $user);
        $userWallet->refresh();

        // Assert
        $this->assertEquals(0, $userWallet->balance);
        $this->assertEquals($user->id, $userWallet->user_id);
    }
}
