<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;

class Voucher extends BaseModel
{
    public static function boot(): void
    {
        parent::boot();

        self::creating(function (Voucher $model) {
            if ($model->code === null) {
                $model->code = self::generateCode();
            }
        });
    }

    protected $casts = [
        'value' => 'float',
    ];

    /**
     * code format : xxx-xxx
     *
     * @return string
     */
    public static function generateCode(): string
    {
        $code = '***-***';
        $randomChars = [];

        $howManyCharsWeNeed = Str::substrCount(haystack: $code, needle: '*');
        for ($i = 0; $i < $howManyCharsWeNeed; $i++) {
            $randomChars[] = Str::random(1);
        }

        $code = Str::replaceArray('*', $randomChars, $code);

        if (self::whereCode($code)->exists()) {
            return self::generateCode();
        }

        return $code;
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
