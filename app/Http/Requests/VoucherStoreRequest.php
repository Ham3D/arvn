<?php

namespace App\Http\Requests;

use App\Enums\VoucherTypesEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VoucherStoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'type' => ['required', Rule::in(VoucherTypesEnum::casesAsArray())],
            'value' => ['required'],
            'quantity' => ['int'],
            'expire_at' => ['date'],
            'code' => ['sometimes'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
