<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VoucherIndexRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'active' => ['bool'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
