<?php

namespace App\Exceptions;

use Exception as MainException;
use Illuminate\Http\JsonResponse;
use Throwable;

class Exception extends MainException
{
    protected $message;

    public function __construct(?string $message = '', int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        if ($message !== '') {
            $this->message = $message;
        } else {
            $this->message = null;
        }
    }

    /**
     * Handle an response error json.
     *
     * @param  int  $code  http status code
     * @param  string  $codeMessage
     * @return JsonResponse
     */
    protected function error(int $code, string $codeMessage): JsonResponse
    {
        $outputArray = [
            'code' => $code,
            'message' => $codeMessage,
        ];

        return response()->json($outputArray, $code);
    }
}
