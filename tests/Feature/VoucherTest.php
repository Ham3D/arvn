<?php

namespace Tests\Feature;

use App\Enums\VoucherTypesEnum;
use App\Exceptions\ExpirationDateIsAlreadyPassedException;
use App\Exceptions\VoucherCodeExistException;
use App\Exceptions\VoucherIsNotAvailableException;
use App\Models\User;
use App\Models\Voucher;
use App\Voucher\VoucherService;
use App\Wallet\WalletService;
use Tests\TestCase;

class VoucherTest extends TestCase
{
    /**
     * @test
     *
     * @return void
     *
     * @throws ExpirationDateIsAlreadyPassedException
     * @throws VoucherCodeExistException
     */
    public function itGetVouchersList(): void
    {
        // Arrange
        $voucherGenerator = new VoucherService();
        $voucherGenerator->generate(type: VoucherTypesEnum::FixedAmount, value: 1000);
        $voucher = Voucher::first();

        // Act
        $response = $this->get(route('voucher.index'));

        // Assert
        $response->assertJsonFragment(['code' => $voucher->code]);
        $response->assertJsonFragment(['type' => $voucher->type]);
        $response->assertJsonFragment(['value' => $voucher->value]);
        $response->assertJsonFragment(['expires_at' => $voucher->expires_at]);
    }

    /**
     * @throws ExpirationDateIsAlreadyPassedException
     * @throws VoucherCodeExistException
     * @test
     */
    public function itGetActiveVouchersList(): void
    {
        // Arrange
        $voucherGenerator = new VoucherService();
        $voucherGenerator->generate(type: VoucherTypesEnum::FixedAmount, value: 1000);
        // hackish way, update voucher expire_at to yesterday
        $voucher = Voucher::first();
        $voucher->update(['expires_at' => now()->subDay()]);

        // Act
        $response = $this->get(route('voucher.index', [
            'active' => true,
        ]));

        // Assert
        $response->assertJsonCount(0, 'data');
    }

    /**
     * @throws ExpirationDateIsAlreadyPassedException
     * @throws VoucherCodeExistException
     * @test
     */
    public function itGetOneVoucher(): void
    {
        // Arrange
        $voucherGenerator = new VoucherService();
        $voucherGenerator->generate(type: VoucherTypesEnum::FixedAmount, value: 1000);
        $voucher = Voucher::first();

        // Act
        $response = $this->get(route('voucher.show', ['id' => $voucher->id]));

        // Assert
        $response->assertJsonFragment(['code' => $voucher->code]);
        $response->assertJsonFragment(['type' => $voucher->type]);
        $response->assertJsonFragment(['value' => $voucher->value]);
        $response->assertJsonFragment(['expires_at' => $voucher->expires_at]);
    }

    /**
     * @throws ExpirationDateIsAlreadyPassedException
     * @throws VoucherCodeExistException
     * @test
     */
    public function itThrowExceptionWhenTryingToGetInvalidVoucher(): void
    {
        // Arrange
        $voucherGenerator = new VoucherService();
        $voucherGenerator->generate(type: VoucherTypesEnum::FixedAmount, value: 1000);

        // Act
        $response = $this->get(route('voucher.show', ['id' => 10 /* invalid id*/]));

        // Assert
        $response->assertJsonFragment(['message' => 'VoucherNotFound']);
        $response->assertStatus(400);
    }

    /**
     * @test
     */
    public function itStoreNewVouchers(): void
    {
        // Arrange
        $params = [
            'type' => VoucherTypesEnum::FixedAmount->name,
            'value' => 100,
            'quantity' => 10,
            'expire_at' => now()->addDay(),
        ];

        // Act
        $this->post(route('voucher.store'), $params);
        $voucher = Voucher::first();

        // Assert
        $this->assertEquals(100, $voucher->value);
        $this->assertEquals(VoucherTypesEnum::FixedAmount->name, $voucher->type);
        $this->assertEquals(10, $voucher->quantity);
    }

    /**
     * @test
     */
    public function itUpdateVoucher(): void
    {
        // Arrange
        $params = [
            'type' => VoucherTypesEnum::FixedAmount->name,
            'value' => 100,
            'quantity' => 10,
            'expire_at' => now()->addDay(),
        ];
        $this->post(route('voucher.store'), $params);
        $voucher = Voucher::first();

        // Act
        $params = [
            'expire_at' => now()->addYear(),
            'status' => false,
            'quantity' => 22,
        ];
        $this->put(route('voucher.update', ['id' => $voucher->id]), $params);

        // Assert
        $voucher->refresh();

        $this->assertEquals(false, $voucher->status);
        $this->assertEquals(22, $voucher->quantity);
        $this->assertEquals(now()->addYear(), $voucher->expires_at);
    }

    /**
     * @test
     */
    public function itDestroyVoucher(): void
    {
        // Arrange
        $params = [
            'type' => VoucherTypesEnum::FixedAmount->name,
            'value' => 100,
            'quantity' => 10,
            'expire_at' => now()->addDay(),
        ];
        $this->post(route('voucher.store'), $params);
        $voucher = Voucher::first();

        // Act
        $this->delete(route('voucher.destroy', ['id' => $voucher->id]), $params);

        // Assert
        $voucher->refresh();

        $this->assertEquals(false, $voucher->status);
    }

    /**
     * @test
     *
     * @throws ExpirationDateIsAlreadyPassedException
     * @throws VoucherCodeExistException
     * @throws VoucherIsNotAvailableException
     */
    public function itCheckIfVoucherIsAvailableToClaimed(): void
    {
        // Arrange
        $voucherService = new VoucherService();
        $voucherService->generate(type: VoucherTypesEnum::FixedAmount, value: 1000);
        $voucher = Voucher::first();

        // Act
        $status = $voucherService->checkIfVoucherIsAvailableToClaim(voucher: $voucher);

        // Assert
        $this->assertTrue($status);
    }

    /**
     * @throws ExpirationDateIsAlreadyPassedException
     * @throws VoucherCodeExistException
     * @test
     */
    public function itClaimVoucher(): void
    {
        // Arrange
        $user = User::factory()->create();
        $voucherService = new VoucherService();
        $voucherService->generate(type: VoucherTypesEnum::FixedAmount, value: 1000);
        $voucher = Voucher::first();

        // Act
        $response = $this->get(route('voucher.claim', [
            'id' => $voucher->id,
            'phone' => $user->phone,
            'voucher' => $voucher->code,
        ]));

        // Assert
        $response->assertStatus(200);
    }

    /**
     * @throws ExpirationDateIsAlreadyPassedException
     * @throws VoucherCodeExistException
     * @test
     */
    public function itProperlyCalculateWalletsAfterVoucherClaim(): void
    {
        // Arrange
        $user = User::factory()->create();
        $voucherService = new VoucherService();
        $voucherService->generate(type: VoucherTypesEnum::FixedAmount, value: 1000);
        $voucher = Voucher::first();

        // Act
        $this->get(route('voucher.claim', [
            'id' => $voucher->id,
            'phone' => $user->phone,
            'voucher' => $voucher->code,
        ]));

        // Assert
        // check voucher claim details
        // system wallet should be negative amount of voucher
        $systemWallet = WalletService::getSystemWallet();
        $this->assertEquals($voucher->value * -1, $systemWallet->balance);
        // system wallet should be positive amount of voucher
        $userWallet = WalletService::getUserWallet(user: $user);
        $this->assertEquals($voucher->value, $userWallet->balance);
    }

    /**
     * @test
     *
     * @throws VoucherCodeExistException
     * @throws ExpirationDateIsAlreadyPassedException
     */
    public function itReportUsersClaimedVoucher(): void
    {
        // Arrange
        $users = User::factory()->count(10)->create();

        $voucherService = new VoucherService();
        $voucherService->generate(type: VoucherTypesEnum::FixedAmount, value: 1000, quantity: 10);
        $voucher = Voucher::first();

        // Act
        foreach ($users as $user) {
            $this->get(route('voucher.claim', [
                'id' => $voucher->id,
                'phone' => $user->phone,
                'voucher' => $voucher->code,
            ]));
        }

        $response = $this->get(route('report', ['code' => $voucher->code]));

        // Assert
        $response->assertJsonFragment(['balance' => 1000]);
        $response->assertJsonFragment(['phone' => $users[1]->phone]);
        $response->assertJsonFragment(['id' => $users[0]->id]);
        $response->assertJsonCount(10, 'data');
    }
}
