<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransactionsRequest;
use App\Http\Requests\WalletCreditRequest;
use App\Http\Resources\UserWalletResource;
use App\Wallet\WalletService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class WalletController extends Controller
{
    /**
     * @param  WalletCreditRequest  $request
     * @param  WalletService  $walletService
     * @return UserWalletResource
     */
    public function balance(WalletCreditRequest $request, WalletService $walletService): UserWalletResource
    {
        return $walletService->getUserBalance(request: $request);
    }

    /**
     * @param  TransactionsRequest  $request
     * @param  WalletService  $walletService
     * @return AnonymousResourceCollection
     */
    public function transactions(TransactionsRequest $request, WalletService $walletService): AnonymousResourceCollection
    {
        return $walletService->userTransactions(request: $request);
    }
}
