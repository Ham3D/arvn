<?php

namespace App\Enums;

use App\Traits\EnumTrait;

enum WalletTypesEnum: string
{
    use EnumTrait;

    case User = 'User';
    case System = 'System';
}
