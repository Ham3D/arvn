<?php

namespace App\Services;

use App\Exceptions\UserNotFoundException;
use App\Models\User;

class UserService
{
    public static function getUser(string $phone): User
    {
        return User::wherePhone($phone)->firstOr(fn () => throw new UserNotFoundException());
    }
}
