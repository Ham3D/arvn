<?php

namespace App\Wallet;

use App\Enums\TransactionTypesEnum;
use App\Enums\WalletTypesEnum;
use App\Exceptions\SystemIntegrityException;
use App\Exceptions\UserNotFoundException;
use App\Http\Requests\TransactionsRequest;
use App\Http\Requests\WalletCreditRequest;
use App\Http\Resources\TransactionResource;
use App\Http\Resources\UserWalletResource;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use App\Services\UserService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class WalletService
{
    public static function getUserBalance(WalletCreditRequest $request): UserWalletResource
    {
        $user = User::wherePhone($request->get('phone'))->firstOr(fn () => throw new UserNotFoundException());

        $userWallet = self::getUserWallet(user: $user);

        return new UserWalletResource(resource: $userWallet);
    }

    public static function getUserWallet(User $user): Wallet
    {
        return Wallet::firstOrCreate(['user_id' => $user->id], ['balance' => 0]);
    }

    public static function userTransactions(TransactionsRequest $request): AnonymousResourceCollection
    {
        $user = UserService::getUser(phone: $request->get('phone'));
        $userWallet = self::getUserWallet(user: $user);
        $transactions = TransactionService::getWalletTransactions(wallet: $userWallet);

        return TransactionResource::collection(resource: $transactions);
    }

    public static function getSystemWallet(): Wallet
    {
        return Wallet::firstOrCreate(['type' => WalletTypesEnum::System->name], ['balance' => 0]);
    }

    /**
     * @return bool
     */
    public static function checkTotalSystemIntegrity(): bool
    {
        $withDrawsSum = Transaction::whereType(TransactionTypesEnum::Withdraw->name)->sum('amount');
        $depositsSum = Transaction::whereType(TransactionTypesEnum::Deposit->name)->sum('amount');

        return $withDrawsSum === $depositsSum;
    }

    public static function checkWalletsIntegrity(): void
    {
        Wallet::chunk(100, function ($chunk) {
            /** @var Wallet $wallet */
            foreach ($chunk as $wallet) {
                $transactions = Transaction::whereWalletId($wallet->id);
                $userBalance = 0;

                /** @var Transaction $transaction */
                foreach ($transactions as $transaction) {
                    switch ($transaction->type) {
                        case TransactionTypesEnum::Deposit->name:
                            $userBalance += $transaction->amount;
                            break;
                        case TransactionTypesEnum::Withdraw->name:
                            $userBalance -= $transaction->amount;
                            break;
                    }
                }

                if ($userBalance !== $wallet->balance) {
                    throw new SystemIntegrityException();
                }
            }
        });
    }
}
