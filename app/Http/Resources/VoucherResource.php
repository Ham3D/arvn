<?php

namespace App\Http\Resources;

use App\Models\Voucher;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Voucher */
class VoucherResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var Voucher $voucher */
        $voucher = $this->resource;

        return [
            'id' => $voucher->id,
            'code' => $voucher->code,
            'type' => $voucher->type,
            'value' => $voucher->value,
            'quantity' => $voucher->quantity,
            'status' => (bool) $voucher->status,
            'expires_at' => $voucher->expires_at,
            'created_at' => $voucher->created_at,
        ];
    }
}
