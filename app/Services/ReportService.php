<?php

namespace App\Services;

use App\Exceptions\VoucherNotFoundException;
use App\Http\Requests\VoucherReportRequest;
use App\Http\Resources\UserResource;
use App\Models\Voucher;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ReportService
{
    public function voucherReport(VoucherReportRequest $request): AnonymousResourceCollection
    {
        $voucher = Voucher::whereCode($request->get('code'))->firstOr(fn () => throw new VoucherNotFoundException());
        $voucher->load(['users', 'users.wallet']);

        return UserResource::collection(resource: $voucher->users);
    }
}
