<?php

namespace App\Http\Controllers;

use App\Http\Requests\VoucherReportRequest;
use App\Services\ReportService;

class ReportController extends Controller
{
    public function __invoke(VoucherReportRequest $request, ReportService $reportService)
    {
        return $reportService->voucherReport(request: $request);
    }
}
