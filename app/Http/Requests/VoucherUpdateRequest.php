<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VoucherUpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'expire_at' => ['date'],
            'quantity' => ['int'],
            'status' => ['bool'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
