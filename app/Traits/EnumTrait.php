<?php

namespace App\Traits;

trait EnumTrait
{
    public static function casesAsArray(): array
    {
        $cases = [];
        foreach (self::cases() as $case) {
            $cases[] = $case->name;
        }

        return $cases;
    }
}
