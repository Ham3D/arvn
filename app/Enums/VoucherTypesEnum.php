<?php

namespace App\Enums;

use App\Traits\EnumTrait;

enum VoucherTypesEnum: string
{
    use EnumTrait;

    case FixedAmount = 'FixedAmount';
}
