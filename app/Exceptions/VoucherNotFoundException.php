<?php

namespace App\Exceptions;

use Illuminate\Http\JsonResponse;

class VoucherNotFoundException extends Exception
{
    public function render(): JsonResponse
    {
        return $this->error(400, 'VoucherNotFound');
    }
}
