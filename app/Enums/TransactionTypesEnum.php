<?php

namespace App\Enums;

use App\Traits\EnumTrait;

enum TransactionTypesEnum: string
{
    use EnumTrait;

    case Deposit = 'Deposit';
    case Withdraw = 'Withdraw';
}
