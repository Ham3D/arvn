<?php

namespace App\Voucher;

use App\Enums\VoucherTypesEnum;
use App\Exceptions\ExpirationDateIsAlreadyPassedException;
use App\Exceptions\VoucherAlreadyUsedException;
use App\Exceptions\VoucherCodeExistException;
use App\Exceptions\VoucherIsNotAvailableException;
use App\Exceptions\VoucherNotFoundException;
use App\Http\Requests\ClaimVoucherRequest;
use App\Http\Requests\VoucherIndexRequest;
use App\Http\Requests\VoucherUpdateRequest;
use App\Http\Resources\VoucherResource;
use App\Models\User;
use App\Models\UserVoucherPivot;
use App\Models\Voucher;
use App\Services\UserService;
use App\Wallet\TransactionService;
use Cache;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class VoucherService
{
    public function getVouchers(VoucherIndexRequest $request): AnonymousResourceCollection
    {
        $query = Voucher::query();

        $query = $this->handleGetVouchersActiveFilter(query: $query, request: $request);

        return VoucherResource::collection(resource: $query->get());
    }

    /**
     * @param    $query
     * @param  VoucherIndexRequest  $request
     * @return Builder
     */
    private function handleGetVouchersActiveFilter($query, VoucherIndexRequest $request): Builder
    {
        if ($request->has('active') && $request->get('active')) {
            $query->where(function ($query) {
                $query->orWhereNull('expires_at');
                $query->orWhere('expires_at', '>', now());
            });
        }

        return $query;
    }

    /**
     * @param  int  $id
     * @return VoucherResource
     */
    public function getVoucher(int $id): VoucherResource
    {
        $voucher = Voucher::whereId($id)->firstOr(fn () => throw new VoucherNotFoundException());

        return new VoucherResource($voucher);
    }

    public function update(VoucherUpdateRequest $request, int $id): bool
    {
        $voucher = Voucher::whereId($id)->firstOr(fn () => throw new  VoucherNotFoundException());

        $update = [];

        if ($request->has('expire_at')) {
            $update['expires_at'] = $request->get('expire_at');
        }

        if ($request->has('quantity')) {
            $update['quantity'] = $request->get('quantity');
        }

        if ($request->has('status')) {
            $update['status'] = $request->get('status');
        }

        $voucher->update($update);

        return true;
    }

    public function destroy(int $id): bool
    {
        $voucher = Voucher::whereId($id)->firstOr(fn () => throw new  VoucherNotFoundException());
        $voucher->update(['status' => false]);

        return true;
    }

    /**
     * @throws VoucherCodeExistException
     * @throws ExpirationDateIsAlreadyPassedException
     */
    public function generate(
        VoucherTypesEnum $type,
        float $value,
        int $quantity = 1,
        Carbon $expireAt = null,
        string $code = null
    ): bool {
        $this->checkCode(code: $code);
        $this->checkExpirationDate(expire_at: $expireAt);

        $voucher = new Voucher();

        if ($code !== null) {
            $voucher->code = $code;
        }

        $voucher->value = $value;
        $voucher->type = $type->name;
        $voucher->quantity = $quantity;

        if ($expireAt !== null) {
            $voucher->expires_at = $expireAt;
        }

        $voucher->save();

        return true;
    }

    /**
     * @throws VoucherCodeExistException
     */
    private function checkCode(string $code = null): void
    {
        if ($code !== null && Voucher::whereCode($code)->exists()) {
            throw new VoucherCodeExistException();
        }
    }

    /**
     * @throws ExpirationDateIsAlreadyPassedException
     */
    private function checkExpirationDate(Carbon $expire_at = null): void
    {
        if ($expire_at !== null && $expire_at->isPast()) {
            throw new ExpirationDateIsAlreadyPassedException();
        }
    }

    /**
     * @param  ClaimVoucherRequest  $request
     * @return bool
     *
     * @throws VoucherAlreadyUsedException
     * @throws VoucherIsNotAvailableException
     */
    public function claimVoucher(ClaimVoucherRequest $request): bool
    {
        $user = UserService::getUser(phone: $request->get('phone'));
        $voucher = $this->getVoucherByCode(code: $request->get('voucher'));
        // check if user already used voucher
        $this->checkIfUserUsedVoucher(user: $user, voucher: $voucher);
        $this->checkIfVoucherIsAvailableToClaim(voucher: $voucher);

        // user can use voucher
        UserVoucherPivot::create([
            'user_id' => $user->id,
            'voucher_id' => $voucher->id,
        ]);

        TransactionService::addClaimedVoucherTransactions(voucher: $voucher, user: $user);

        return true;
    }

    /**
     * @param  string  $code
     * @return mixed
     */
    public function getVoucherByCode(string $code): Voucher
    {
        return Cache::remember("voucher:$code", now()->addMinutes(10), function () use ($code) {
            return Voucher::whereCode($code)->firstOr(fn () => throw new VoucherNotFoundException());
        });
    }

    /**
     * @throws VoucherAlreadyUsedException
     */
    public function checkIfUserUsedVoucher(User $user, Voucher $voucher): void
    {
        if (UserVoucherPivot::whereUserId($user->id)->whereVoucherId($voucher->id)->exists()) {
            throw new VoucherAlreadyUsedException();
        }
    }

    /**
     * @throws VoucherIsNotAvailableException
     */
    public function checkIfVoucherIsAvailableToClaim(Voucher $voucher): bool
    {
        if (! $voucher->status) {
            throw new VoucherIsNotAvailableException();
        }

        $currentUsedVoucher = UserVoucherPivot::whereVoucherId($voucher->id)->count();

        if ($voucher->quantity <= $currentUsedVoucher) {
            $voucher->update(['status' => false]);
            // remove voucher from cache
            Cache::forget("voucher:$voucher->code");
            throw new VoucherIsNotAvailableException();
        }

        return true;
    }
}
