<?php

use App\Enums\WalletTypesEnum;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(model: User::class)->nullable()->constrained();
            $table->enum('type', WalletTypesEnum::casesAsArray())->default(WalletTypesEnum::User->name);
            $table->decimal(column: 'balance', total: 20)->default(0);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('wallets');
    }
};
