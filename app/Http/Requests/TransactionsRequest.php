<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'phone' => ['required', 'int'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
